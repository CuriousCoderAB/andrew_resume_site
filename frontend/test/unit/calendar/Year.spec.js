import Year from '@/components/utilities/calendar/Year'
import { mount } from '@vue/test-utils'
import moment from 'moment'

describe('Calendar - Year', () => {
  const $t = (message) => message

  test('If there are no messages, the flash message is not shown', () => {
    const wrapper = mount(
      Year,
      {
        mocks: {
          $t
        },
        props: {
          selectedDate: moment('2020-01-01').toDate(),
          locale: 'en'
        }
      }
    )

    console.log(moment('2020-01-01').toDate())

    const allMessages = wrapper.findAll('.flash-message')
    expect(allMessages.length).toBe(0)
  })
})