export default {
  toggleDarkMode({ commit, state }) {
    commit('toggleDarkMode')

    const htmlElement = document.documentElement
    const mode = state.darkMode ? 'dark' : 'light'

    htmlElement.classList.add('transition')
    window.setTimeout(() => {
      htmlElement.classList.remove('transition')
    }, 1000)

    htmlElement.setAttribute('data-theme', mode)
  }
}
