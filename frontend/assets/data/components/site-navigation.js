export default {
  home: {
    to: '/',
    class: 'bar-logo',
    exact: true
  },
  portfolio: {
    to: '/portfolio',
    class: 'push-right bar-item'
  },
  about: {
    to: '/about',
    class: 'bar-item'
  },
  contact: {
    to: '/contact',
    class: 'bar-item'
  }
}
