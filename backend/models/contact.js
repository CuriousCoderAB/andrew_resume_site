const MongoModel = require('./base/MongoModel.js')

class ContactRepository extends MongoModel {
  constructor() {
    const collection = "contact"
    super(collection)
  }
}


module.exports = new ContactRepository();