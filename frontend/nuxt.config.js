import webpack from 'webpack'

export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    title: 'Andrew Bilenduke - Full Stack Web Developer',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || 'A dedicated full stack web developer.'
      },
      {
        hid: 'keywords',
        name: 'keywords',
        content: 'HTML, CSS, JavaScript, PHP, Python, Node.js, Vue.js, Laravel, Flask, Express, SQL, NoSQL, Full Stack, Web Developer, For Hire'
      },
      {
        hid: 'author',
        name: 'author',
        content: 'Andrew Bilenduke'
      },
      {
        hid: 'og:type',
        property: 'og:type',
        content: 'website'
      },
      {
        hid: 'og:title',
        property: 'og:title',
        content: 'Andrew Bilenduke - Full Stack Web Developer'
      },
      {
        hid: 'og:description',
        property: 'og:description',
        content: process.env.npm_package_description || 'A dedicated full stack web developer.'
      },
      {
        hid: 'og:image',
        property: 'og:image',
        content: 'https://andrewbilendukecdn.nyc3.digitaloceanspaces.com/resume/andrew_bilenduke_meta.jpg'
      },
      {
        hid: 'og:url',
        property: 'og:url',
        content: 'https://andrewbilenduke.com'
      },
      {
        hid: 'twitter:card',
        property: 'twitter:card',
        content: 'summary_large_image'
      },
      {
        hid: 'twitter:url',
        property: 'twitter:url',
        content: 'https://andrewbilenduke.com'
      },
      {
        hid: 'twitter:title',
        property: 'twitter:title',
        content: 'Andrew Bilenduke - Full Stack Web Developer'
      },
      {
        hid: 'twitter:description',
        property: 'twitter:description',
        content: process.env.npm_package_description || 'A dedicated full stack web developer.'
      },
      {
        hid: 'twitter:image',
        property: 'twitter:image',
        content: 'https://andrewbilendukecdn.nyc3.digitaloceanspaces.com/resume/andrew_bilenduke_meta.jpg'
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  env: {
    baseUrl: process.env.BASE_URL || 'http://localhost:3000',
    apiUrl: process.env.API_URL || 'http://localhost:3000',
    googleMapsKey: process.env.GOOGLE_MAPS_KEY || ''
  },
  watchers: {
    webpack: {
      aggregateTimeout: 300,
      poll: 1000
    }
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#E27D60' },
  /*
   ** Global CSS
   */
  css: ['@/assets/scss/app.scss'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: ['~/plugins/i18n.js'],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    '@nuxtjs/stylelint-module',
    [
      '@nuxtjs/moment', {
        defaultLocale: 'en',
        locales: ['fr']
      }
    ]
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv'
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    proxy: true,
    prefix: '/api/',
    baseURL: process.env.apiUrl
  },
  proxy: {
    '/api/': { target: process.env.apiUrl, pathRewrite: {'^/api/': ''}, changeOrigin: true }
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
    plugins: [
      new webpack.ProvidePlugin({
        _: 'lodash',
        loader: 'html-loader'
      })
    ]
  }
}
