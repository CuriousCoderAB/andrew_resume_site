export default {
  toggleMenu(state) {
    state.showMenu = !state.showMenu
  },
  toggleDarkMode(state) {
    state.darkMode = !state.darkMode
  },
  setDarkMode(state, value) {
    state.darkMode = value
  }
}
