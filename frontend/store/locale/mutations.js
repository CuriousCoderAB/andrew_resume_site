import availableLocales from '../../locales/available.json'

export default {
  setAvailableLocales(state) {
    state.locales = Object.keys(availableLocales)
  },
  setLocale(state, locale) {
    state.locale = locale
  }
}
