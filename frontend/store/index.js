export const state = () => ({
  isRequestSending: false,
  flashMessages: []
})

export const getters = {
  getIsRequestSending(state) {
    return state.isRequestSending
  }
}

export const mutations = {
  toggleIsRequestSending(state) {
    state.isRequestSending = !state.isRequestSending
  },
  addFlashMessage(state, message) {
    state.flashMessages.push({
      body: message.body,
      level: message.level,
      isAutoRemove: message.isAutoRemove
    })
  },
  removeFlashMessage(state, message) {
    const index = state.flashMessages.indexOf(message)
    if (index > -1) {
      state.flashMessages.splice(index, 1)
    }
  }
}

export const actions = {
  async nuxtServerInit({ dispatch, commit }, { req }) {
    const cookieparser = process.server ? require('cookieparser') : undefined
    await commit('locale/setAvailableLocales')
    if (req.headers.cookie) {
      const parsed = cookieparser.parse(req.headers.cookie)
      let selectedLocale = null

      try {
        selectedLocale = JSON.parse(parsed.locale)
        await dispatch('locale/setLocale', selectedLocale)
      } catch (error) {
        console.log('Welcome to my site!')
      }

      try {
        const theme = JSON.parse(parsed.theme)
        if (theme === 'dark') {
          await commit('site/setDarkMode', true)
        }
      } catch {
        console.log('Dark theme is not set, try it out in the menu.')
      }
    }
  }
}
