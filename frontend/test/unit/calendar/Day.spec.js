import Day from '@/components/utilities/calendar/Day'
import { mount } from '@vue/test-utils'
import moment from 'moment'

describe('Calendar - Day', () => {
  const $t = (message) => message

  test('If there are no messages, the flash message is not shown', () => {
    const wrapper = mount(
      Day, 
      {
        mocks: {
          $t
        },
        props: {
          selectedDate: moment('2020-01-01').toDate(),
          locale: 'en'
        }
      }
    )

    const allMessages = wrapper.findAll('.flash-message')
    expect(allMessages.length).toBe(0)
  })
})