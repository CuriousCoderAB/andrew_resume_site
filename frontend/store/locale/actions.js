export default {
  async setLocale({ commit, state }, locale) {
    if (state.locales.includes(locale)) {
      await commit('setLocale', locale)
      this.app.i18n.locale = locale
      this.app.$moment.locale(locale)
    }
  }
}
