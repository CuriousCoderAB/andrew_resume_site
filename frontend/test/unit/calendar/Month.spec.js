import Month from '@/components/utilities/calendar/Month'
import { shallowMount } from '@vue/test-utils'
import moment from 'moment'

describe('Calendar - Month', () => {
  const $t = (message) => message

  test('If there are no messages, the flash message is not shown', () => {
    const wrapper = shallowMount(
      Month, 
      {
        mocks: {
          $t
        },
        props: {
          selectedDate: moment('2020-01-01').toDate(),
          locale: 'en'
        }
      }
    )

    const allMessages = wrapper.findAll('.flash-message')

    expect(allMessages.length).toBe(0)
  })
})