require('dotenv').config();

const createError = require('http-errors');
const express = require('express');
const cors = require('cors')
const path = require('path');
const morgan = require('morgan');
const { contact } = require('./routes');

const application = express();

corsOptions = {
  origin: process.env.FRONTEND_APP_URL,
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}
application.use(cors(corsOptions))

application.use(morgan('dev'));

application.use(express.json());
application.use(express.urlencoded({ extended: false }));
application.use(express.static(path.join(__dirname, 'public')));

application.use('/contact', contact);

// catch 404 and forward to error handler
application.use(function(req, res, next) {
  next(createError(404));
});

// error handler
application.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: err
  });
});

module.exports = application;
