import FlashMessages from '@/components/site/FlashMessages'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'

const localVue = createLocalVue()

localVue.use(Vuex)

describe('FlashMessages', () => {
  let store = null;
  let state = null;
  const $t = (message) => message

  test('If there are no messages, the flash message is not shown', () => {
    state = () => {
      return {
        flashMessages: null
      }
    },
    store = new Vuex.Store({
      state
    })
    const wrapper = shallowMount(
      FlashMessages, {
        store,
        localVue
      }
    )

    const allMessages = wrapper.findAll('.flash-message')

    expect(allMessages.length).toBe(0)
  })

  test('If there is one message, it is shown', () => {
    state = () => {
      return {
        flashMessages: [
          {
            body: 'TEST',
            level: 'success',
            isAutoRemove: false
          }
        ]
      }
    },
    store = new Vuex.Store({
      state
    })
    const wrapper = shallowMount(
      FlashMessages, {
        mocks: {
          $t
        },
        store,
        localVue
      }
    )

    const allMessages = wrapper.findAll('.flash-message')

    expect(allMessages.length).toBe(1)
    expect(allMessages.isVisible()).toBe(true)
  })

  test('If there are multiple messages, they are all shown', () => {
    state = () => {
      return {
        flashMessages: [
          {
            body: 'TEST - 1',
            level: 'success',
            isAutoRemove: false
          },
          {
            body: 'TEST - 2',
            level: 'success',
            isAutoRemove: false
          },
          {
            body: 'TEST - 3',
            level: 'success',
            isAutoRemove: false
          }
        ]
      }
    },
    store = new Vuex.Store({
      state
    })
    const wrapper = shallowMount(
      FlashMessages, {
        mocks: {
          $t
        },
        store,
        localVue
      }
    )

    const allMessages = wrapper.findAll('.flash-message')

    expect(allMessages.length).toBe(3)
    expect(allMessages.isVisible()).toBe(true)
  })
})