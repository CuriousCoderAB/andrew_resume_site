const MongoClient = require('mongodb').MongoClient;

class MongoModel {
  constructor(collection) {
    const username = process.env.MONGODB_USERNAME
    const password = process.env.MONGODB_PASSWORD
    const host = process.env.MONGODB_HOST
    const port = process.env.MONGODB_PORT
    const database = process.env.MONGODB_DATABASE
    
    this.url = `mongodb://${username}:${password}@${host}:${port}`;
    this.database = database;
    this.collection = collection;
  }
  async insert(data) {
    const database = this.database;
    const collection = this.collection;

    await MongoClient.connect(this.url, function(err, db) {
      if (err) throw err;
      const dbo = db.db(database);
      dbo.collection(collection).insertOne(data, function(err, result) {
        if (err) throw err;
        db.close();
        return result;
      });
    });
  }

  async all() {
    const database = this.database;
    const collection = this.collection;

    await MongoClient.connect(this.url, function(err, db) {
      if (err) throw err;
      const dbo = db.db(database);
      dbo.collection(collection).find({}).toArray(function(err, result) {
        if (err) throw err;
        db.close();
        return result;
      });
    });
  }
}

module.exports = MongoModel