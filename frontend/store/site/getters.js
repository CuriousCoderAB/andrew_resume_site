export default {
  getShowMenu(state) {
    return state.showMenu
  },
  getDarkMode(state) {
    return state.darkMode
  }
}
