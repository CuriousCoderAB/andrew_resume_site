export default {
  sendContactMessage({ commit }, formValues) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        commit('toggleIsRequestSending', null, { root: true })
        this.$axios.setBaseURL(process.env.API_URL)
        this.$axios
          .$post('contact', formValues)
          .then(() => {
            commit(
              'addFlashMessage',
              {
                body: 'contact.flash.send.success',
                level: 'success',
                isAutoRemove: true
              },
              { root: true }
            )
            resolve()
          })
          .catch((error) => {
            commit(
              'addFlashMessage',
              {
                body: 'contact.flash.send.error',
                level: 'danger',
                isAutoRemove: true
              },
              { root: true }
            )
            reject(error)
          })
          .finally(() => commit('toggleIsRequestSending', null, { root: true }))
      }, 1000)
    })
  }
}
